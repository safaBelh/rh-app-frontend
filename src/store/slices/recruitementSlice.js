import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    checkDropSubList : false
  
};

export const recruitementSlice = createSlice({
  name: "recruitement",
  initialState,
  reducers: {
    handleDropSubList : (state , action) => {
        state.checkDropSubList = action.payload.checkDropSubList
    }
   
  },
});

// Action creators are generated for each case reducer function
export const { handleDropSubList } = recruitementSlice.actions;

export default recruitementSlice.reducer;
