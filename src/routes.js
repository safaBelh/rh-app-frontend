import { Suspense, Fragment, lazy } from "react";
import { Routes, Route } from "react-router-dom";
import LoadingScreen from "./components/LoadingScreen";
import MainLayout from "./layouts/MainLayout";

export const renderRoutes = (routes = []) => (
  <Suspense fallback={<LoadingScreen />}>
    <Routes>
      {routes.map((route, i) => {
        const Layout = route.layout || Fragment;
        const Component = route.component;

        return (
          <Route
            key={i}
            path={route.path}
            element={
              <Layout>
                <Component />
              </Layout>
            }
          />
        );
      })}
    </Routes>
  </Suspense>
);

const routes = [
  {
    path: "/",
    layout: MainLayout,
    component: lazy(() => import("./views/Home")),
  },
  {
    path: "/createJob",
    layout: MainLayout,
    component: lazy(() => import("./views/CreateJob")),
  },
  {
    path: "/companyLandingPage",
    layout: MainLayout,
    component: lazy(() => import("./views/CompanyLandingPage")),
  },
  {
    path: "/:jobId",
    layout: MainLayout,
    component: lazy(() => import("./views/JobPosition")),
  },
  {
    path: "/application",
    layout: MainLayout,
    component: lazy(() => import("./views/Application/Application")),
  },
];

export default routes;
